#ifndef _ENS_FUN_H_
#define _ENS_FUN_H_
#include "base.h"

typedef Nat Ens;

Ens ev();

Ens insert(Ens e, Nat a);

Ens del_bin (Ens e, Nat a);

Bool dans(Ens e, Nat a);

Nat card(Ens e);

Nat bdix (Nat cell);

Nat first_value (Nat cell);

#endif
