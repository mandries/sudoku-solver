#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "base.h"
#include "ens_fun.h"

// Ensemble of solutions has the following type:
// 	9 8 7 6 5 4 3 2 1	are stocked in a binary integer
// 	1 1 1 1 1 1 1 1 1	<-- having this form

// Create a void ensemble = creating a nil value.
Ens ev()
	{
	return (Ens) 0;
	}

// Insert value "a" into ensemble of solution "e"
// I'm not sure we will need this function at this moment (06 dec 2008)
Ens insert(Ens e, Nat a)
	{
	if (a==1)
		return e+1;
	else
		return insert(e/2, a-1)*2 + e%2;
	// alternative code
	// 	return 
	// 		a==1 	? e+1
	// 			: insert(e/2, a-1)*2 + e%2;
	}


//Delete a value "value" from the solution ensemble "e"
//"value" here is of (ENS) type (100000 or 100 or 1 etc...)
Ens del_bin (Ens e, Nat value)
	{
	return e-value;
	}

//Verifies if a value "a" (in Base 10) is contained in a solution ensemble "e"
Bool dans(Ens e, Nat a)
	{
	if (a==1)
		return e%2;
	else
		return dans(e/2, a-1);
	}

// A function that returns the number of possible values left in a solution ensemble "e"
Nat card(Ens e)
	{
	if (e==0)
		return 0;
	else
		return card(e/2)+e%2;
	}


// Function to transform (Ens) into Base10 (only if there is only one "1" in the cell value of type (Ens))
Nat bdix (Nat cell)
	{
	int counter=1;
	if (cell==0)
		return 0;
	else if (cell==1)
		return 1;
	else
		{
		while ((cell%2!=1) && (counter<10))
			{
			cell=cell/2;
			counter++;
			}
		return counter;
		}
	}

// Return the first possibility out of several (out of a cell) (in Ens form)
Nat first_value (Nat cell)
	{
	// Calculating the first value... START
	Nat counter=1;

	if (card(cell)>0)
		{
		while ((cell%2!=1) && (counter<1024))
			{
			cell=cell/2;
			counter=counter*2;
			}
		// Calculating the first value, case card(cell)>0 ... DONE
 		return counter;
		}		
	return 0;
	}
