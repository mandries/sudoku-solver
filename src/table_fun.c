#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "table_fun.h"
#include "ens_fun.h"
extern table t_orig;

// Creation and initialization of a new Sudoku table
table create_table()
	{
	table t;
	int i;
	for (i=0;i<dim;i++)
		{
		t.t[i]=511;
		t.flag[i]=0;
		}
	//511 = 111 111 111 in base 2
	//flag=0 if cell not yet analysed
	//flag=1 if cell already analysed
	return t;
	}

// Find the number of the cell with minimum nr of possibilities left
// "case la plus contrainte"
int card_min(table t)
	{
	int i;
	int min_nr_sol = 9;
	int min_case = 0;
	for (i=0;i<dim;i++)
		{
		if ((card(t.t[i])<min_nr_sol) && (t.flag[i]==0))
			{
 			min_nr_sol=card(t.t[i]);
			min_case=i;
			}
		}
	return min_case;
	}

// Count the number of cells already filled
int total_done(table t)
	{
	int i;
	int counter=0;
	for (i=0;i<dim;i++)
		if (t.flag[i]==1)
			counter++;
	return counter;
	}


// Deletion of a value from the ensemble of solutions of interconnected cases
// The input variable "value" must be of type (Ens)
table del_val_cases(table t, int value, int case_nr)
	{
	//Deleting value from all the cells related to  [(case_nr/9)+1] [(case_nr%9)+1]
	int l,c;
	// l = indique la ligne
	// c = indique la colonne
	int i,j;	// i, j used in the loops
	l = case_nr/9; 	// l = case_nr/sqrt(dim);
	c = case_nr%9; //  c = case_nr%sqrt(dim);
		
	// Delete "value" from ensembles on the same line as our cell (except our input cell)
	for (i=0;i<9;i++)
		{
		if (i!=c)	//if not our cell
			{
			// test if a value is present in the target ensemble of solutions
			if (dans(t.t[l*9+i], bdix(value))==1)
				//If "value" of (Ens) type, then simple substractino "-"
				t.t[l*9+i]=del_bin(t.t[l*9+i],value);
			}
		}	
		
	// Delete value from ensembles on the same column as our cell (except for our cell)	
	for (i=0;i<9;i++)
		{
		if (i!=l)	//if not our cell
			{
			// test if a value is present in the target ensemble of solutions
			if (dans(t.t[i*9+c], bdix(value))==1)
				t.t[i*9+c]=del_bin(t.t[i*9+c],value);
			}
		}
		
	// Delete value from ensembles in the same 3x3 square as our cell (except for our cell)	
	for (i=(l/3)*3;i<(l/3)*3+3;i++)
		for (j=(c/3)*3;j<(c/3)*3+3;j++)
			{
			if (i*9+j!=l*9+c)	//if not our cell
				{
				// test if a value is present in the target ensemble of solutions
				if (dans(t.t[i*9+j], bdix(value))==1)
					t.t[i*9+j]=del_bin(t.t[i*9+j],value);
				}
			}
	return t;
	}


// Print a Sudoku table
void print_table(table t)
	{
	int i;
	for (i=0;i<dim;i++)
		{	
		//Division line (horizontal) between 3x3 squares
		if (i%27==0)
			{
 			printf("\n|===========||===========||===========|\n");
			if (card(t.t[i])==1)
				printf("| %d ",bdix(t.t[i]));
			else 
				printf("|   ");
			}
		//Division line (horizontal) between each line
		else if (i%9==0)
			{
			printf("\n|---+---+---||---+---+---||---+---+---|\n");
			if (card(t.t[i])==1)
				{
				if (t.t[i]==t_orig.t[i])
					{
					printf("|%c[34;1m %d %c[0;0m",27,bdix(t.t[i]),27);
					}
				else
					{
					printf("| %d ",bdix(t.t[i]));
					}
				}
			else 
				printf("|   ");
			}
		// Division line (vertical) between 3x3 squares
		else if (i%3==0)
			{
			if (card(t.t[i])==1)
				{
				if (t.t[i]==t_orig.t[i])
					{
					printf("||%c[1;34m %d %c[0;0m",27,bdix(t.t[i]),27);
					}
				else
					{
					printf("|| %d ",bdix(t.t[i]));
					}
				}
			else 
				printf("||   ");
			}
		// Closing the cells at the end of the line
		else if (i%9==8)
			{
			if (card(t.t[i])==1)
				{
				if (t.t[i]==t_orig.t[i])
					{
					printf("|%c[1;34m %d %c[0;0m|",27,bdix(t.t[i]),27);
					}
				else
					{
					printf("| %d |",bdix(t.t[i]));
					}
				}
			else 
				printf("|   |");
			}
		// Other interior cells
		else 
			{
			if (card(t.t[i])==1)
				printf("| %d ",bdix(t.t[i]));
			else
				printf("|   ");
			}
		}
	//Closing line
 	printf("\n|===========||===========||===========|\n");
	}
