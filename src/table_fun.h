#ifndef _TABLE_FUN_H_
#define _TABLE_FUN_H_
// #define side 9
// dim = (side*side)
#define dim 81

typedef struct
	{
 	int t[dim];
	int flag[dim];
	} table;

table create_table();

int card_min(table t);

table del_val_cases(table t_in, int value, int case_nr);

void print_table(table t);

int total_done(table t);

#endif
