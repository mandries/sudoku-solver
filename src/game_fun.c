#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "table_fun.h"
#include "ens_fun.h"
#include "game_fun.h"
static int solution_nr;
static int complexity;
table t_orig;

// Analytic algo
void analytic (table t_in)
	{
	table t_out;
	t_out=create_table();
	int i;	// "i" used in the loop
	int case_nr=card_min(t_in);
	int value;
	// Starting a branch of analytic search:
	
	//Case 1: Solution found (print solution)
	if (total_done(t_in)==81)
		{
		solution_nr++;
 		printf("\nSolution found (nr. %d):\n",solution_nr);
		print_table(t_in);
		}

	//Case 2: Error found (do nothing or print "branch exhausted")
	else if (card(t_in.t[case_nr])==0)
		{
  		// printf("\nNo more solutions for this branch of essais");
		}

	//Case 3: Everything works fine, only one untreated_cell (with flag==0) to complete at a time
	else if (card(t_in.t[case_nr])==1)
		{
		complexity++;
		//deletion of value from related cases
		t_in=del_val_cases(t_in, t_in.t[case_nr], case_nr);
		//Changing cell's flag value to "analysed"
		t_in.flag[case_nr]=1;
		//Continue analysing the modified table
 		analytic(t_in);
		}

	//Case 4: Blocked (more solutions available) : try them all	
	else 
		{
		if (card(t_in.t[case_nr])>1)
			{
			// Calculating complexity
 			while(card(t_in.t[case_nr]>0))
 				{	
				t_out=t_in;
 				// try first possibility out of several
				// Inserting the first value out of several into the output
				t_out.t[case_nr]=first_value(t_in.t[case_nr]);
				
				// Propagating the effect
 				t_out=del_val_cases(t_out, t_out.t[case_nr], case_nr);
				// Changing cell's flag value to "analysed"
 				t_out.flag[case_nr]=1;

				// Ripping of the first value from the original table
				t_in.t[case_nr]=t_in.t[case_nr]-first_value(t_in.t[case_nr]);

				//Recalculate card_min(t_in)
 				case_nr=card_min(t_in);

				//Analytic try of the new sudoku table
				analytic(t_out);	
 				}
			//No need to set the cell's flag to "analysed" here
			}
		}
	}


// Type void, because we will not return a table, but automatically calculate the solutions inside "create_table"
void real_time_create_table()
	{
	table t;
	t=create_table();
	int line,column, value;
	int sudoku_value=1;

	do
		{
		line=13; column=13; sudoku_value=1;

		do	{
			value=13;
			printf("\nWhat value do you want to insert into the table? (0 - erase value; 10 - finish): ");
			scanf("%d",&value);
			if ((value<0) || (value>10))					
				printf("\nValue must be between 1 and 9 (or 0 to erase a cell value, 10 to finish introducing values)");
			} 
		while((value<0) || (value>10));
		
		// Reading line
		while (((line<0) || (line>8)) && value!=10)
			{
			if (value!=0)
				printf("\nPlease enter the row/line, where the value must be inserted: ");
			else
				printf("\nPlease enter the row/line, where the value should be deleted: ");
			scanf("%d",&line);
			line--;
			if ((line<0) || (line>8))
					printf("\nLine number must be between 1 and 9");
			}

		// Reading column
		while (((column<0) || (column>8)) && value!=10)
			{
			if (value!=0)
				printf("\nPlease enter the column, where the value must be inserted: ");
			else
				printf("\nPlease enter the column, where the value should be deleted: ");
			scanf("%d",&column);
			column--;
			if ((column<0) || (column>8))
				printf("\nColumn number must be between 1 and 9");
			}

		//Erase a cell
		if (value==0)
			{
			t.t[line*9+column]=511;
			system("clear");
			print_table(t);
			}

		//If value introduced
		else if ((value>0) && (value<10))
			{
			sudoku_value=1;
			while (value!=1)
				{
				sudoku_value=sudoku_value*2;	
				value--;;
				}
			//Setting cell value
			t.t[line*9+column]=sudoku_value;
			//Set cell flag as "treated" -> No need. Otherwise we won't be able to check if the table has any solutions
			// No need to propagate the input value through the table at this stage
			system("clear");
			printf("\nValue inserted: %d",t.t[line*9+column]);
			print_table(t);
			}
		}while (value!=10);
	printf("\nDone introducing values.");

	//Saving the original table
	t_orig=t;

	//Calculating the solutions
	analytic(t);
	printf("\nTable complexity (number of values guessed and analyzed for all the possible variants): %d\n",complexity);
	}

//Load a sudoku table from file
void load_table()
 	{
	int line,column,value,nbr_chiffres,i;
	table t;
	Path filename;
	int sudoku_value=1;
	printf("\nPlease introduce the path to the world to load:");
	scanf("%s",filename.N);
	printf("\nReading filename... DONE");
	FILE* myfile=NULL;
	// Initializing myfile pointer... DONE
	myfile=fopen(filename.N,"r");
	// File opened... DONE
	if (myfile!=NULL)
		{
		t=create_table();
		fscanf(myfile,"%d",&nbr_chiffres);
		printf("%d",nbr_chiffres);
		printf("\nRead point coordinates and value\n");
		
		for(i=0;i<nbr_chiffres;i++)
 			{
			fscanf(myfile,"%d",&line);
			fscanf(myfile,"%d",&column);
			fscanf(myfile,"%d",&value);
 			//Coordinates read in 1-9 range; real table range 0-8
 			
			sudoku_value=(1<<value -1);
			t.t[(line-1)*9+(column-1)]=sudoku_value;
					
			}
		// Closing the file
		
		// int fclose(myfile *fp);
		fclose (myfile);
		
		//Print the original Sudoku table
		system("clear");
		printf("\nOriginal table:");
		print_table(t);	

		//Saving the original table
		t_orig=t;

		//Calculating the solutions
		analytic(t);
		printf("\nTable complexity (number of values guessed and analyzed for all the possible variants): %d\n",complexity);
		}
	else
		{
		printf("\nFile doesn't exist\n");
		main();
		}
		
	}

main()
	{
	// Initialiser ou charger un tableau "t"
	table t;
	int choice=0;
	do
		{
		//Initializing the complexity and number of solutions
		complexity=0;
	 	solution_nr=0;
		printf("\nPlease select game mode:");
		printf("\n1. Create a table in real-time");
		printf("\n2. Load a table from file");
		printf("\n3. Quit\n");


		scanf("%d",&choice);
	
		
		switch (choice)
			{
			case 1:	real_time_create_table();
				break;
			case 2: load_table();
				break;
			case 3: exit(0);
				break;
			default: printf("\nInvalid choice.");
				break;
			}
		} while (choice!=3);
	}
